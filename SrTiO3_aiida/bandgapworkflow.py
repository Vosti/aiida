from aiida.orm import Code,StructureData,Dict,KpointsData,Group
from aiida_pseudo.data.pseudo.psf import PsfData
from aiida.tools.data.array.kpoints.legacy import get_explicit_kpoints_path as legacy_path
from aiida_siesta.workflows.bandgap import BandgapWorkChain
from aiida.engine import run,submit

#Code
codename = 'siesta4.1_b4@CHPC'
code = Code.get_from_string(codename)

#Structure
alat = float(input("Enter lattice constant:")) #Lattice constant in Angstroms
cell = [[alat, 0., 0.,],[0., alat, 0.,],[0., 0., alat],]

symbol_A = 'Sr' #input("Give symbol of the A cation:")

#ATiO3
s = StructureData(cell=cell)
s.append_atom(position=(0.000*alat,0.000*alat,0.000*alat),symbols=[symbol_A])
s.append_atom(position=(0.500*alat,0.500*alat,0.500*alat),symbols=['Ti'])
s.append_atom(position=(0.500*alat,0.500*alat,0.000*alat),symbols=['O'])
s.append_atom(position=(0.500*alat,0.000*alat,0.500*alat),symbols=['O'])
s.append_atom(position=(0.000*alat,0.500*alat,0.500*alat),symbols=['O'])

#PSEUDO
family = Group.get(label='perovskite')
pseudos_dict = family.get_pseudos(structure=s)


## K-POINTS ##
bandskpoints=KpointsData()
kpp = [('\Gamma',  (0.000,  0.000, 0.000), 'X', (1.000,  0.000, 0.000), 22),
('X', (1.000,  0.000, 0.000), 'M', (1.0, 1.0, 0.0), 22),
('M', (1.0, 1.0, 0.0), 'Gamma', (0.0, 0.0, 0.0), 33),
('\Gamma', (0.0, 0.0, 0.0), 'R', (1.0, 1.0, 1.0), 39),
('R', (1.0, 1.0, 1.0), 'X', (1.0, 0.0, 0.0), 33)]
tmp=legacy_path(kpp)
bandskpoints.set_kpoints(tmp[3])
bandskpoints.labels=tmp[4]

options = Dict(
        dict = {
            'custom_scheduler_commands': '#PBS -l mem={}gb\n#PBS -q normal\n#PBS -P MATS862\nexport OMP_NUM_THREADS={}'.format(
                           118,12),
            'resources': {
                          'num_machines': 2,
                          'num_cores_per_machine': 24,
                          'num_mpiprocs_per_machine':2,
                          },
            'max_wallclock_seconds': 24*60*60-1,
             })

#Submitting the calculation
builder = BandgapWorkChain.get_builder()
builder.options = options
builder.code = code
builder.structure = s
builder.parameters = load_node(28)
builder.pseudos = pseudos_dict
builder.basis = load_node(29)
builder.bandskpoints = bandskpoints

#run the calculation in an interactive way
results = run(builder)

#submit calculation to the daemon
#calc = submit(builder)
