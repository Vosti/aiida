from aiida.orm import Dict

#Parameters
parameters = Dict(dict={
  "mesh-cutoff": "400 Ry",
  "dm-tolerance": 0.0001,
  "xc-functional": "LDA",
  "xc-authors": "CA",
  "max-scfiterations" : 300,
  'dm-numberpulay': 3,
  'dm-mixingweight' : 0.2,
  'electronic-temperature': "100 K",

  "%block chemicalspecieslabel":
    """
    1  38  Sr
    2  22  Ti
    3  8   O
%endblock chemicalspecieslabel""",

    "%block kgrid_monkhorst_pack": 
    """
    6   0   0    0.5
    0   6   0    0.5
    0   0   6    0.5
%endblock kgrid_monkhorst_pack""",

    "%block pslmax":
    """
   Sr    3
   Ti    3
    O    3
%endblock pslmax""",

})


#BASIS SET
# basis_dict = {
# 'pao-basistype':'split',
# 'pao-splitnorm': 0.25,
# 'pao-energyshift': '0.020 Ry',
# '%block pao-basis-sizes':
# """
# Sr    DZP
# Ti    DZP
# O     DZP
# %endblock pao-basis-sizes""",
# }

#BASIS SET
basis_dict = {
'%block pao-basis':
"""
Sr   5      1.64000
 n=4   0   1   E   155.00000     6.00000
     6.49993
     1.00000
 n=5   0   2   E   149.48000     6.50000
     6.99958     5.49957
     1.00000     1.00000
 n=4   1   1   E   148.98000     5.61000
     6.74964
     1.00000
 n=5   1   1   E     4.57000     1.20000
     4.00000
     1.00000
 n=4   2   1   E   146.26000     6.09000
     6.63062
     1.00000
Ti    5      1.91
 n=3    0    1   E     93.95      5.20
   5.69946662616249
   1.00000000000000
 n=3    1    1   E     95.47      5.20
   5.69941339465994
   1.00000000000000
 n=4    0    2   E     96.47      5.60
   6.09996398975307        5.09944363262274
   1.00000000000000        1.00000000000000
 n=3    2    2   E     46.05      4.95
   5.94327035784617        4.70009988294302
   1.00000000000000        1.00000000000000
 n=4    1    1   E      0.50      1.77
   3.05365979938936
   1.00000000000000
O     3     -0.28
 n=2    0    2   E     40.58      3.95
   4.95272270428712        3.60331408800389
   1.00000000000000        1.00000000000000
 n=2    1    2   E     36.78      4.35
   4.99990228025066        3.89745395068600
   1.00000000000000        1.00000000000000
 n=3    2    1   E     21.69      0.93
   2.73276990670788
   1.00000000000000
%endblock pao-basis""",
 }


basis = Dict(dict=basis_dict)


print(parameters.store(), "parameters for Sr")
print(basis.store(), "basis for Sr")
